<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Demo Kalender</title>
<style>
table, td, th {
  border: 1px solid #ddd;
  text-align: left;
}
table {
  border-collapse: collapse;
  width: 100%;
}
th, td {
  padding: 15px;
}
tr:nth-child(even) {background-color: #f2f2f2;}
</style>
</head>
<body>
<h1>Demo Kalender</h1>
<?php
$weekdays = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];
$months = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
$cur_date = date('Y-m-d', strtotime('today'));

for ($i=0; $i < 3; $i++) {
  $date = date('Y-m-d', strtotime($cur_date. ' + ' . $i.' month'));
  $cur_year = date('Y', strtotime($date));
  $cur_month = date('n', strtotime($date));
	$cur_day = date('d', strtotime($date));
  $month_str = $months[$cur_month-1];
  $num_day = date('N', mktime(0, 0, 0, $cur_month, 1, $cur_year));
  $total_days = date('t', mktime(0, 0, 0, $cur_month, 1, $cur_year));

  echo "\n<h2>$month_str $cur_year</h2>";

  echo "\n<table>\n<tr><td>";
  echo implode($weekdays, "</td><td>");
  echo "</td></tr>\n<tr>";

  for ($j = 1; $j < $num_day; $j++) {
    echo "<td>  </td>";
  }

  for ($day = 1; $day <= $total_days; $day++) {
		if ($cur_day == $day && $cur_date == $date) {
			echo "<td style='background-color:#02d35f'>";
		}
		else {
			echo "<td>";
		}
    echo $day . "</td>";

    if ($num_day == 7) {
      echo "</tr>\n";
      if (($day + 1) <= $total_days) {
        echo "<tr>";
      }
      $num_day = 1;
    }
    else {
      $num_day++;
    }
  }

  if ($num_day != 1) {
    for ($j = $num_day; $j <= 7; $j++) {
      echo "<td>  </td>";
    }
  }

  echo "</table>\n";
}
?>
</body>
</html>
